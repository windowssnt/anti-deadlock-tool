

void (WINAPI * TInitializeCriticalSection)(
	_Out_ LPCRITICAL_SECTION lpCriticalSection
);

void WINAPI MInitializeCriticalSection(
	_Out_ LPCRITICAL_SECTION lpCriticalSection
)
	{
	DWORD tid = GetCurrentThreadId();
	if (tid == MyThread)
		return TInitializeCriticalSection(lpCriticalSection);

	PutThreads(true);
	TInitializeCriticalSection(lpCriticalSection);
	O o;
	o.Type = OBJECT_TYPE::TYPE_CS;
	o.LastChangeFrom = FindThread(tid);
	o.h = (HANDLE)lpCriticalSection;
	objs.w()->operator[]((HANDLE)lpCriticalSection) = o;
	UpdateDialog();
	TInitializeCriticalSection(lpCriticalSection);
	}




void (WINAPI * TDeleteCriticalSection)(
	_Out_ LPCRITICAL_SECTION lpCriticalSection
	);

void WINAPI MDeleteCriticalSection(
	_Out_ LPCRITICAL_SECTION lpCriticalSection
)
	{
	DWORD tid = GetCurrentThreadId();
	if (tid == MyThread)
		return TDeleteCriticalSection(lpCriticalSection);

	if (objs.r()->find((HANDLE)lpCriticalSection) != objs.r()->end())
		{
		objs.w()->erase((HANDLE)lpCriticalSection);
		UpdateDialog();
		}
	TDeleteCriticalSection(lpCriticalSection);
	}



void (WINAPI * TEnterCriticalSection)(
	_Out_ LPCRITICAL_SECTION lpCriticalSection
	);

void WINAPI MEnterCriticalSection(
	_Out_ LPCRITICAL_SECTION lpCriticalSection
)
	{
	DWORD tid = GetCurrentThreadId();
	if (tid == MyThread)
		return TEnterCriticalSection(lpCriticalSection);


	auto objf = FindObject((HANDLE)lpCriticalSection);
	if (objf)
		{
		auto thr = FindThread(tid);
		if (thr)
			{
			thr->WaitingList.clear();
			thr->WaitingList.push_back(objf);
			thr->WaitingAll = true;
			thr->WaitTimeout = INFINITE;
			objf->Owner = thr;

			// And now update
			PostMessage(hDlg, WM_USER + 1, tid, 0);

			TEnterCriticalSection(lpCriticalSection);
			thr->WaitingList.clear();
			objf->UpdateState();
			return;
			}
		}
	TEnterCriticalSection(lpCriticalSection);
	}

	

void (WINAPI * TLeaveCriticalSection)(
	_Out_ LPCRITICAL_SECTION lpCriticalSection
	);

void WINAPI MLeaveCriticalSection(
	_Out_ LPCRITICAL_SECTION lpCriticalSection
)
	{
	DWORD tid = GetCurrentThreadId();
	if (tid == MyThread)
		return TLeaveCriticalSection(lpCriticalSection);


	auto objf = FindObject((HANDLE)lpCriticalSection);
	if (objf)
		objf->Owner = 0;
	TLeaveCriticalSection(lpCriticalSection);
	}



bool HookCS()
	{
	// CreateEvent
	if (MH_CreateHook(&InitializeCriticalSection, &MInitializeCriticalSection, reinterpret_cast<LPVOID*>(&TInitializeCriticalSection)) != MH_OK) return false;
	if (MH_EnableHook(&InitializeCriticalSection) != MH_OK) return  false;

	if (MH_CreateHook(&DeleteCriticalSection, &MDeleteCriticalSection, reinterpret_cast<LPVOID*>(&TDeleteCriticalSection)) != MH_OK) return false;
	if (MH_EnableHook(&DeleteCriticalSection) != MH_OK) return  false;

	if (MH_CreateHook(&EnterCriticalSection, &MEnterCriticalSection, reinterpret_cast<LPVOID*>(&TEnterCriticalSection)) != MH_OK) return false;
	if (MH_EnableHook(&EnterCriticalSection) != MH_OK) return  false;

	if (MH_CreateHook(&LeaveCriticalSection, &MLeaveCriticalSection, reinterpret_cast<LPVOID*>(&TLeaveCriticalSection)) != MH_OK) return false;
	if (MH_EnableHook(&LeaveCriticalSection) != MH_OK) return  false;

	return true;
	}

