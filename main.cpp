#include <winsock2.h>
#include <windows.h>
#include <wininet.h>
#include <process.h>
#include <commctrl.h>
#include <stdio.h>
#include <shellapi.h>
#include <string>
#include <vector>
#include <tchar.h>
#include <memory>
#include <thread>
#include <functional>
#include <map>
using namespace std;


#pragma comment(lib,"ws2_32.lib")
#pragma comment(lib,"wininet.lib")
#pragma comment(lib,"crypt32.lib")
#pragma comment(lib,"Comctl32.lib")

#pragma comment(linker,"\"/manifestdependency:type='win32' \
name='Microsoft.Windows.Common-Controls' version='6.0.0.0' \
processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")


#include "minhook\\include\\MinHook.h"
#include "func.hpp"

HINSTANCE hDLL = 0;
DWORD MyThread = 0;

enum class OBJECT_TYPE
	{
	TYPE_EVENT = 0,
	TYPE_MUTEX = 1,
	TYPE_CS = 2,
	};

enum class OBJECT_STATE
	{
	TYPE_EV_RESET = 0,
	TYPE_EV_SET = 1,
	};
extern DWORD(WINAPI *TWaitForSingleObject)(HANDLE, DWORD);

struct T* FindThread(DWORD h);
extern DWORD(WINAPI *TSetEvent)(HANDLE);

RWMUTEX StateUpdate;
struct  O
	{
	HANDLE h;
	wstring Name;
	OBJECT_TYPE Type;
	OBJECT_STATE State;

	// Event Specific
	BOOL ManualReset = false;

	// Mutex Specifc
	mutable struct T* Owner = 0;

	mutable struct T* LastChangeFrom = 0;

	void UpdateState()
		{
		StateUpdate.LockWrite();
		if (Type == OBJECT_TYPE::TYPE_EVENT)
			{
			auto res = TWaitForSingleObject(h, 0);
			if (res == WAIT_TIMEOUT) State = OBJECT_STATE::TYPE_EV_RESET;
			if (res == WAIT_OBJECT_0)
				{
				State = OBJECT_STATE::TYPE_EV_SET;
				if (ManualReset == false)
					TSetEvent(h);
				}
			}
	/*	if (Type == OBJECT_TYPE::TYPE_MUTEX)
			{
			auto res = TWaitForSingleObject(h, 0);
			if (res == WAIT_OBJECT_0) Owner = FindThread(GetCurrentThreadId());
			}

*/		
		StateUpdate.ReleaseWrite();
		}
	};
struct  T
	{
	wstring Name;
	DWORD id;
	vector<struct O*> WaitingList;
	BOOL WaitingAll = false;
	DWORD WaitTimeout = INFINITE;
	};



tlock<std::map<HANDLE, O>> objs;
tlock<std::map<DWORD, T>> threads;


O* FindObject(HANDLE h)
	{
	if (objs.direct().empty())
		return 0;
	auto objf = objs.r()->find(h);
	if (objf == objs.r()->end())
		return 0;
	return (O*)&objf->second;
	}

T* FindThread(DWORD h)
	{
	auto objf = threads.r()->find(h);
	if (objf == threads.r()->end())
		return 0;
	return (T*)&objf->second;
	}


HWND hDlg = 0;


void UpdateDialog()
	{
	if (!hDlg)
		return;

	HWND hL1 = GetDlgItem(hDlg, 901);
	HWND hL2 = GetDlgItem(hDlg, 902);
	ListView_DeleteAllItems(hL1);
	ListView_DeleteAllItems(hL2);

	wchar_t tt[1000] = { 0 };
	size_t i = 0;
	for (auto o = objs.r()->begin(); o != objs.r()->end(); o++)
		{
		_stprintf_s(tt, 1000, L"%zu", i + 1);
		LV_ITEM lv0 = { LVIF_TEXT | LVIF_PARAM,0,0,0,0,tt,0,0,0 };
		lv0.lParam = (LPARAM)&o->second;
		int L = ListView_InsertItem(hL1, &lv0);
		if (o->second.Type == OBJECT_TYPE::TYPE_EVENT) { ListView_SetItemText(hL1, L, 1, L"Event"); }
		if (o->second.Type == OBJECT_TYPE::TYPE_MUTEX) { ListView_SetItemText(hL1, L, 1, L"Mutex"); }
		if (o->second.Type == OBJECT_TYPE::TYPE_CS) { ListView_SetItemText(hL1, L, 1, L"Critical Section"); }

		// Event
		if (o->second.Type == OBJECT_TYPE::TYPE_EVENT)
			{
			if (o->second.State == OBJECT_STATE::TYPE_EV_RESET) 
				{ 
				_stprintf_s(tt, 1000, L"Reset (%s)", o->second.ManualReset ? L"Manual" : L"Automatic");
				}
			if (o->second.State == OBJECT_STATE::TYPE_EV_SET) 
				{ 
				_stprintf_s(tt, 1000, L"Set (%s)", o->second.ManualReset ? L"Manual" : L"Automatic");
				}
			ListView_SetItemText(hL1, L, 4, tt);
			}
		if (o->second.Type == OBJECT_TYPE::TYPE_MUTEX)
			{
			tt[0] = 0;
			if (o->second.Owner)
				_stprintf_s(tt, 1000, L"(%p)", o->second.h);
			ListView_SetItemText(hL1, L, 4, tt);
			}


		_stprintf_s(tt, 1000, L"%p", o->second.h);
		ListView_SetItemText(hL1, L, 3, tt);
		ListView_SetItemText(hL1, L, 2, (LPWSTR)o->second.Name.c_str());

		_stprintf_s(tt, 1000, L"%u", o->second.LastChangeFrom->id);
		ListView_SetItemText(hL1, L, 5, tt);

		i++;
		}

	for (int j = 0; j < 5; j++)
		AutoSizeLVColumn(hL1, j);


	i = 0;
	for (auto o = threads.r()->begin(); o != threads.r()->end(); o++)
		{
		_stprintf_s(tt, 1000, L"%zu", i + 1);
		LV_ITEM lv0 = { LVIF_TEXT | LVIF_PARAM,0,0,0,0,tt,0,0,0 };
		lv0.lParam = (LPARAM)&o->second;
		int L = ListView_InsertItem(hL2, &lv0);
		swprintf_s(tt, 1000, L"%u", o->first);
		ListView_SetItemText(hL2, L, 1, tt);

		if (!o->second.WaitingList.empty())
			{
			wstring t;
			for (size_t y = 0; y < o->second.WaitingList.size(); y++)
				{
				TCHAR e[100];
				swprintf_s(e, 100, L"%p ", o->second.WaitingList[y]->h);
				t += e;
				}
			_stprintf_s(tt, 1000, L"Waiting on (%s) [%s] (%u)", t.c_str(),o->second.WaitingAll ? L"All" : L"Any",o->second.WaitTimeout);
			ListView_SetItemText(hL2, L, 3, tt);
			}

		i++;
		}

	for (int j = 0; j < 4; j++)
		AutoSizeLVColumn(hL2, j);
	}

void PutThreads(bool NoU = false)
	{
	DWORD hh = GetCurrentThreadId();
	auto found = FindThread(hh);
	if (!found)
		{
		T t;
		t.id = hh;
		threads.w()->operator[](hh) = t;
		}
	if (!NoU)
		PostMessage(hDlg, WM_USER + 1, hh, NoU);
	}


#include "waitfunctions.hpp"
#include "eventfunctions.hpp"
#include "csfunctions.hpp"
#include "mutexfunctions.hpp"

TEVENT<> ev1;
INT_PTR CALLBACK D_DP(HWND hh, UINT mm, WPARAM , LPARAM ll)
	{
	HWND hL1 = GetDlgItem(hh, 901);
	HWND hL2 = GetDlgItem(hh, 902);
	switch (mm)
		{
		case WM_USER + 1:
			{
			// Update Threads
			UpdateDialog();
			return 0;
			}

		case WM_NOTIFY:
			{
			NMHDR* n = (NMHDR*)ll;
			if (n->code == NM_RCLICK)
				{
				int L = ListView_GetNextItem(n->hwndFrom, -1, LVIS_SELECTED);
				if (L == -1)
					return 0;
				LPARAM lp = ListGetParam(n->hwndFrom, L);
				POINT p;
				GetCursorPos(&p);
				
				if (n->hwndFrom == hL1)
					{
					O* o = (O*)lp;
					if (!o)
						return 0;
					if (o->Type == OBJECT_TYPE::TYPE_EVENT)
						{
						int tcmd = TrackPopupMenu(GetSubMenu(LoadMenu(hDLL, L"MENU_POPUPS"), 0), TPM_RIGHTALIGN | TPM_LEFTBUTTON | TPM_RETURNCMD, p.x, p.y, 0, hh, 0);
						if (tcmd == 1) // Set
							{
							TSetEvent(o->h);
							o->UpdateState();
							UpdateDialog();
							}
						if (tcmd == 2) // Reset
							{
							TResetEvent(o->h);
							o->UpdateState();
							UpdateDialog();
							}
						if (tcmd == 3) // Update
							{
							o->UpdateState();
							UpdateDialog();
							}
						}
					if (o->Type == OBJECT_TYPE::TYPE_MUTEX)
						{
						int tcmd = TrackPopupMenu(GetSubMenu(LoadMenu(hDLL, L"MENU_POPUPS"), 1), TPM_RIGHTALIGN | TPM_LEFTBUTTON | TPM_RETURNCMD, p.x, p.y, 0, hh, 0);
						if (tcmd == 1) // Grab
							{
							TWaitForSingleObject(o->h,2000);
							o->UpdateState();
							UpdateDialog();
							}
						if (tcmd == 2) // Reset
							{
							TReleaseMutex(o->h);
							o->UpdateState();
							UpdateDialog();
							}
						if (tcmd == 3) // Update
							{
							o->UpdateState();
							UpdateDialog();
							}
						}
					}

				}
			return 0;
			}


		case WM_INITDIALOG:
			{
			hDlg = hh;
			ev1.Set();
			PostMessage(hh, WM_SIZE, 0, 0);
			int px = LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES | LVS_EX_DOUBLEBUFFER;

			// Objects
			ListInsertColumn(hL1, 0, LVCF_SUBITEM | LVCF_TEXT | LVCF_WIDTH, 0, 30, L"#", 0, 0, 0, 0);
			ListInsertColumn(hL1, 1, LVCF_SUBITEM | LVCF_TEXT | LVCF_WIDTH, 0, 30, L"Type", 0, 0, 0, 0);
			ListInsertColumn(hL1, 2, LVCF_SUBITEM | LVCF_TEXT | LVCF_WIDTH, 0, 30, L"Name", 0, 0, 0, 0);
			ListInsertColumn(hL1, 3, LVCF_SUBITEM | LVCF_TEXT | LVCF_WIDTH, 0, 30, L"Handle", 0, 0, 0, 0);
			ListInsertColumn(hL1, 4, LVCF_SUBITEM | LVCF_TEXT | LVCF_WIDTH, 0, 30, L"State/Owner", 0, 0, 0, 0);
			ListInsertColumn(hL1, 5, LVCF_SUBITEM | LVCF_TEXT | LVCF_WIDTH, 0, 30, L"Last Change From", 0, 0, 0, 0);
			for (int i = 0; i < 6; i++)
				AutoSizeLVColumn(hL1, i);
			SendMessage(hL1, LVM_FIRST + 54, px, px);

			ListInsertColumn(hL2, 0, LVCF_SUBITEM | LVCF_TEXT | LVCF_WIDTH, 0, 30, L"#", 0, 0, 0, 0);
			ListInsertColumn(hL2, 1, LVCF_SUBITEM | LVCF_TEXT | LVCF_WIDTH, 0, 30, L"ID", 0, 0, 0, 0);
			ListInsertColumn(hL2, 2, LVCF_SUBITEM | LVCF_TEXT | LVCF_WIDTH, 0, 30, L"Name", 0, 0, 0, 0);
			ListInsertColumn(hL2, 3, LVCF_SUBITEM | LVCF_TEXT | LVCF_WIDTH, 0, 30, L"State", 0, 0, 0, 0);
			for (int i = 0; i < 4; i++)
				AutoSizeLVColumn(hL2, i);
			SendMessage(hL2, LVM_FIRST + 54, px, px);

			UpdateDialog();
			return 1;
			}
		case WM_SIZE:
			{
			RECT rc;
			GetClientRect(hh, &rc);
			SetWindowPos(hL1, 0, 0, 0, rc.right, rc.bottom / 2, SWP_SHOWWINDOW);
			SetWindowPos(hL2, 0, 0, rc.bottom/2, rc.right, rc.bottom / 2, SWP_SHOWWINDOW);
			return 1;
			}
		}
	return 0;
	}

void WinThread(void*)
	{
	MyThread = GetCurrentThreadId();
	DialogBox(hDLL, L"DIALOG_1", 0, D_DP);
	}

bool Start2()
	{
	MyThread = GetCurrentThreadId();
	// WaitForSingleObject
	if (MH_CreateHook(&WaitForSingleObject, &MWaitForSingleObject, reinterpret_cast<LPVOID*>(&TWaitForSingleObject)) != MH_OK) return false;
	if (MH_EnableHook(&WaitForSingleObject) != MH_OK) return  false;
	
	// WaitForMultipleObjects
	if (MH_CreateHook(&WaitForMultipleObjects, &MWaitForMultipleObjects, reinterpret_cast<LPVOID*>(&TWaitForMultipleObjects)) != MH_OK) return false;
	if (MH_EnableHook(&WaitForMultipleObjects) != MH_OK) return  false;

	if (!HookEvents())
		return false;
	
	if (!HookMutex())
		return false;

	// CloseHandle
	if (MH_CreateHook(&CloseHandle, &MCloseHandle, reinterpret_cast<LPVOID*>(&TCloseHandle)) != MH_OK) return false;
	if (MH_EnableHook(&CloseHandle) != MH_OK) return  false;

	if (!HookCS())
		return false;


	_beginthread(WinThread, 0, 0);
	ev1.Wait();



	return true;
	}

void End()
	{
	}

extern "C"  __declspec(dllexport) bool __stdcall Start()
	{
	if (MH_Initialize() != MH_OK)
		return FALSE;
	if (!Start2())
		return FALSE;
	return TRUE;
	}

BOOLEAN WINAPI DllMain(IN HINSTANCE hDllHandle,
	IN DWORD     nReason,
	IN LPVOID    )
	{
	BOOLEAN bSuccess = TRUE;


	//  Perform global initialization.

	switch (nReason)
		{
		case DLL_PROCESS_ATTACH:
			hDLL = hDllHandle;
			break;

		case DLL_PROCESS_DETACH:
			End();
			break;
		}


	//  Perform type-specific initialization.



	return bSuccess;
	}