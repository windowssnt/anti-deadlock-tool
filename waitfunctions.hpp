// WaitForSingleObject
DWORD(WINAPI *TWaitForSingleObject)(HANDLE, DWORD);
DWORD  WINAPI MWaitForSingleObject(HANDLE hh, DWORD t)
	{
	DWORD tid = GetCurrentThreadId();
	if (tid == MyThread)
		return TWaitForSingleObject(hh, t);

	PutThreads(true);
	auto objf = FindObject(hh);
	if (objf)
		{
		auto thr = FindThread(tid);
		if (thr)
			{
			thr->WaitingList.clear();
			thr->WaitingList.push_back(objf);
			thr->WaitingAll = true;
			thr->WaitTimeout = t;

			// And now update
			PostMessage(hDlg, WM_USER + 1, tid, 0);

			auto he = TWaitForSingleObject(hh, t);
			thr->WaitingList.clear();

			objf->UpdateState();

			PostMessage(hDlg, WM_USER + 1, tid, 0);
			return he;
			}
		}
	return TWaitForSingleObject(hh, t);
	}


// WaitForSingleObject
DWORD(WINAPI *TWaitForMultipleObjects)(_In_       DWORD  nCount,
	_In_ const HANDLE *lpHandles,
	_In_       BOOL   bWaitAll,
	_In_       DWORD  dwMilliseconds);

DWORD  WINAPI MWaitForMultipleObjects(_In_       DWORD  nCount,
	_In_ const HANDLE *lpHandles,
	_In_       BOOL   bWaitAll,
	_In_       DWORD  dwMilliseconds)
	{
	DWORD tid = GetCurrentThreadId();
	if (tid == MyThread)
		return TWaitForMultipleObjects(nCount,lpHandles,bWaitAll,dwMilliseconds);

	PutThreads(true);

	auto thr = FindThread(tid);
	if (!thr)
		return TWaitForMultipleObjects(nCount, lpHandles, bWaitAll, dwMilliseconds);

	thr->WaitingList.clear();
	thr->WaitingAll = bWaitAll;
	thr->WaitTimeout = dwMilliseconds;
	for (DWORD i = 0; i < nCount; i++)
		{
		auto objf = FindObject(lpHandles[i]);
		thr->WaitingList.push_back(objf);
		}
	// And now update
	PostMessage(hDlg, WM_USER + 1, tid, 0);

	auto res = TWaitForMultipleObjects(nCount, lpHandles, bWaitAll, dwMilliseconds);
	thr->WaitingList.clear();
	for (DWORD i = 0; i < nCount; i++)
		{
		auto objf = FindObject(lpHandles[i]);
		objf->UpdateState();
		}
	PostMessage(hDlg, WM_USER + 1, tid, 0);
	return res;
	}



// CloseHandle
BOOL(WINAPI *TCloseHandle)(HANDLE);
BOOL WINAPI MCloseHandle(HANDLE hh)
	{
	DWORD tid = GetCurrentThreadId();
	if (tid == MyThread)
		return TCloseHandle(hh);
	if (objs.r()->find(hh) != objs.r()->end())
		{
		objs.w()->erase(hh);
		UpdateDialog();
		}
	return TCloseHandle(hh);
	}



