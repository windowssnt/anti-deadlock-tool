// CreateMutex
HANDLE (WINAPI *TCreateMutex)(
	_In_opt_ LPSECURITY_ATTRIBUTES lpMutexAttributes,
	_In_     BOOL                  bInitialOwner,
	_In_opt_ LPCTSTR               lpName);

HANDLE WINAPI MCreateMutex(
	_In_opt_ LPSECURITY_ATTRIBUTES lpMutexAttributes,
	_In_     BOOL                  bInitialOwner,
	_In_opt_ LPCTSTR               lpName
)
	{
	DWORD tid = GetCurrentThreadId();
	if (tid == MyThread)
		return TCreateMutex(lpMutexAttributes, bInitialOwner, lpName);

	PutThreads(true);
	auto h = TCreateMutex(lpMutexAttributes, bInitialOwner, lpName);
	O o;
	o.Type = OBJECT_TYPE::TYPE_MUTEX;
	o.Owner = 0;
	if (lpName)
		o.Name = lpName;
	o.LastChangeFrom = FindThread(tid);
	if (bInitialOwner)
		o.Owner = o.LastChangeFrom;
	o.h = h;
	objs.w()->operator[](h) = o;
	UpdateDialog();
	return h;
	}


// CreateMutexEx
HANDLE(WINAPI *TCreateMutexEx)(
	_In_opt_ LPSECURITY_ATTRIBUTES lpMutexAttributes,
	_In_opt_ LPCTSTR               lpName,
	_In_     DWORD                 dwFlags,
	_In_     DWORD                 dwDesiredAccess);

HANDLE WINAPI MCreateMutexEx(
	_In_opt_ LPSECURITY_ATTRIBUTES lpMutexAttributes,
	_In_opt_ LPCTSTR               lpName,
	_In_     DWORD                 dwFlags,
	_In_     DWORD                 dwDesiredAccess
)
	{
	DWORD tid = GetCurrentThreadId();
	if (tid == MyThread)
		return TCreateMutexEx(lpMutexAttributes, lpName,dwFlags,dwDesiredAccess);

	PutThreads(true);
	auto h = TCreateMutexEx(lpMutexAttributes, lpName, dwFlags, dwDesiredAccess);
	O o;
	o.Type = OBJECT_TYPE::TYPE_MUTEX;
	o.Owner = 0;
	if (lpName)
		o.Name = lpName;
	o.LastChangeFrom = FindThread(tid);
	if (dwFlags & CREATE_MUTEX_INITIAL_OWNER)
		o.Owner = o.LastChangeFrom;
	o.h = h;
	objs.w()->operator[](h) = o;
	UpdateDialog();
	return h;
	}



// OpenMutex
HANDLE(WINAPI *TOpenMutex)(
	_In_ DWORD   dwDesiredAccess,
	_In_ BOOL    bInheritHandle,
	_In_ LPCTSTR lpName);

HANDLE WINAPI MOpenMutex(
	_In_ DWORD   dwDesiredAccess,
	_In_ BOOL    bInheritHandle,
	_In_ LPCTSTR lpName
)
	{
	DWORD tid = GetCurrentThreadId();
	if (tid == MyThread)
		return TOpenMutex(dwDesiredAccess, bInheritHandle, lpName);

	PutThreads(true);
	auto h = TOpenMutex(dwDesiredAccess, bInheritHandle, lpName);
	O o;
	o.ManualReset = 0;
	o.Type = OBJECT_TYPE::TYPE_MUTEX;
	o.Owner = 0;
	if (lpName)
		o.Name = lpName;
	o.LastChangeFrom = FindThread(tid);
	o.h = h;
	objs.w()->operator[](h) = o;
	UpdateDialog();
	return h;
	}


	


// SetEvent
DWORD(WINAPI *TReleaseMutex)(HANDLE);
DWORD  WINAPI MReleaseMutex(HANDLE hh)
	{
	DWORD tid = GetCurrentThreadId();
	if (tid == MyThread)
		return TReleaseMutex(hh);

	PutThreads(true);
	auto objf = FindObject(hh);
	if (objf)
		{
		objf->Owner = 0;
		PostMessage(hDlg, WM_USER + 1, tid, 0);
		}
	return TReleaseMutex(hh);
	}

bool HookMutex()
	{
	// CreateMutex
	if (MH_CreateHook(&CreateMutex, &MCreateMutex, reinterpret_cast<LPVOID*>(&TCreateMutex)) != MH_OK) return false;
	if (MH_EnableHook(&CreateMutex) != MH_OK) return  false;

	// CreateMutexEx
	if (MH_CreateHook(&CreateMutexEx, &MCreateMutexEx, reinterpret_cast<LPVOID*>(&TCreateMutexEx)) != MH_OK) return false;
	if (MH_EnableHook(&CreateMutexEx) != MH_OK) return  false;

	// OpenMutex
	if (MH_CreateHook(&OpenMutex, &MOpenMutex, reinterpret_cast<LPVOID*>(&TOpenMutex)) != MH_OK) return false;
	if (MH_EnableHook(&OpenMutex) != MH_OK) return  false;

	// ReleaseMutex
	if (MH_CreateHook(&ReleaseMutex, &MReleaseMutex, reinterpret_cast<LPVOID*>(&TReleaseMutex)) != MH_OK) return false;
	if (MH_EnableHook(&ReleaseMutex) != MH_OK) return  false;

	return true;
	}