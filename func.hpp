// functions



// Sync items
/*
// reverse_semaphore
class reverse_semaphore
	{
	private:
		HANDLE hE = 0;
		HANDLE hM = 0;
		volatile unsigned long long m = 0;

		reverse_semaphore(const reverse_semaphore&) = delete;
		reverse_semaphore& operator =(const reverse_semaphore&) = delete;

	public:

		reverse_semaphore()
			{
			m = 0;
			hE = TCreateEvent(0, TRUE, TRUE, 0);
			hM = CreateMutex(0, 0, 0);
			}

		~reverse_semaphore()
			{
			TCloseHandle(hM);
			hM = 0;
			TCloseHandle(hE);
			hE = 0;
			}

		void lock()
			{
			WaitForSingleObject(hM, INFINITE);
			m++;
			TResetEvent(hE);
			ReleaseMutex(hM);
			}

		void unlock()
			{
			WaitForSingleObject(hM, INFINITE);
			if (m > 0)
				m--;
			if (m == 0)
				SetEvent(hE);
			ReleaseMutex(hM);
			}

		DWORD Wait(DWORD dw = INFINITE)
			{
			return WaitForSingleObject(hE, dw);
			}

		void WaitAndLock()
			{
			HANDLE h[2] = { hE,hM };
			WaitForMultipleObjects(2, h, TRUE, INFINITE);
			lock();
			ReleaseMutex(hM);
			}
		HANDLE WaitAndBlock()
			{
			HANDLE h[2] = { hE,hM };
			WaitForMultipleObjects(2, h, TRUE, INFINITE);
			return hM;
			}


	};
*/
template <int MR = 1>
class TEVENT
	{
	public:
		HANDLE m = 0;
		TEVENT()
			{
			m = TCreateEvent ? TCreateEvent(0, MR, 0, 0) : CreateEvent(0, MR, 0, 0);
			}
		void Close()
			{
			if (m)
				TCloseHandle(m);
			m = 0;
			}
		DWORD Wait(DWORD i = INFINITE)
			{
			if (m)
				{
				return TWaitForSingleObject ? TWaitForSingleObject(m, i) : WaitForSingleObject(m, i);
				}
			return WAIT_ABANDONED;
			}
		void Set()
			{
			if (TSetEvent) TSetEvent(m); else SetEvent(m);
			}
		void Reset()
			{
			if (TResetEvent) TResetEvent(m); else ResetEvent(m);
			}
		~TEVENT()
			{
			Close();
			}

		TEVENT(const TEVENT&m)
			{
			operator=(m);
			}
		TEVENT(TEVENT&&m)
			{
			operator=(std::forward<TEVENT<MR>>(m));
			}

		TEVENT& operator= (const TEVENT&b)
			{
			Close();
			DuplicateHandle(GetCurrentProcess(), b.m, GetCurrentProcess(), &m, 0, 0, DUPLICATE_SAME_ACCESS);
			return *this;
			}
		TEVENT& operator= (TEVENT&&b)
			{
			Close();
			m = b.m;
			b.m = 0;
			return *this;
			}


	};



extern HANDLE(WINAPI *TCreateMutex)(
	_In_opt_ LPSECURITY_ATTRIBUTES lpMutexAttributes,
	_In_     BOOL                  bInitialOwner,
	_In_opt_ LPCTSTR               lpName);

extern BOOL(WINAPI *TCloseHandle)(HANDLE);
extern DWORD(WINAPI *TWaitForMultipleObjects)(_In_       DWORD  nCount,
	_In_ const HANDLE *lpHandles,
	_In_       BOOL   bWaitAll,
	_In_       DWORD  dwMilliseconds);

extern DWORD(WINAPI *TWaitForSingleObject)(HANDLE, DWORD);
extern DWORD(WINAPI *TReleaseMutex)(HANDLE);


// RWMUTEX
class RWMUTEX
	{
	private:
		HANDLE hChangeMap = 0;
		std::map<DWORD, HANDLE> Threads;

	public:

		RWMUTEX()
			{
			hChangeMap =  TCreateMutex ? TCreateMutex(0, 0, 0) : CreateMutex(0, 0, 0);
			}

		~RWMUTEX()
			{
			TCloseHandle(hChangeMap);
			hChangeMap = 0;
			for (auto& a : Threads)
				TCloseHandle(a.second);
			Threads.clear();
			}

		HANDLE CreateIf(bool KeepReaderLocked = false)
			{
			TWaitForSingleObject(hChangeMap, INFINITE);
			DWORD id = GetCurrentThreadId();
			if (Threads[id] == 0)
				{
				HANDLE e0 = TCreateMutex(0, 0, 0);
				Threads[id] = e0;
				}
			HANDLE e = Threads[id];
			if (!KeepReaderLocked)
				TReleaseMutex ? TReleaseMutex(hChangeMap) : ReleaseMutex(hChangeMap);
			return e;
			}

		void LockRead()
			{
			TWaitForSingleObject(CreateIf(), INFINITE);
			}

		void LockWrite()
			{
			CreateIf(true);

			// Wait for all 
			vector<HANDLE> AllThreads;
			AllThreads.reserve(Threads.size());
			for (auto& a : Threads)
				{
				AllThreads.push_back(a.second);
				}

			TWaitForMultipleObjects((DWORD)AllThreads.size(), AllThreads.data(), TRUE, INFINITE);

			// Reader is locked
			}

		void ReleaseWrite()
			{
			// Release All
			for (auto& a : Threads)
				TReleaseMutex(a.second);
			TReleaseMutex(hChangeMap);
			}

		void ReleaseRead()
			{
			TReleaseMutex(CreateIf());
			}
	};



template <typename T> class tlock
	{
	private:
		mutable T t;
		mutable RWMUTEX m;

		class proxy
			{
			T *const p;
			RWMUTEX* m;
			int me;
			public:
				proxy(T * const _p, RWMUTEX* _m, int _me) : p(_p), m(_m), me(_me) { if (me == 2) m->LockWrite(); else m->LockRead(); }
				~proxy() { if (me == 2) m->ReleaseWrite(); else  m->ReleaseRead(); }
				T* operator -> () { return p; }
				const T* operator -> () const { return p; }
			};

	public:
		template< typename ...Args>
		tlock(Args ... args) : t(args...) {}
		const proxy r() const
			{
			return proxy(&t, &m, 1);
			}
		proxy w()
			{
			return proxy(&t, &m, 2);
			}

		T& direct() { return t; }
		proxy operator -> () { return w(); }
		const proxy operator -> () const { return r(); }

	};


// Helpers
void ListInsertColumn(HWND hL, int i, UINT mask, int fmt, int cx, TCHAR* txt, int txtm, int sub, int iI, int iO)
	{
	LV_COLUMN lc = { mask,fmt,cx,txt,txtm,sub,iI,iO };
	ListView_InsertColumn(hL, i, &lc);
	}

void AutoSizeLVColumn(HWND hL, int j)
	{
	SendMessage(hL, LVM_SETCOLUMNWIDTH, j, LVSCW_AUTOSIZE);
	int a1 = ListView_GetColumnWidth(hL, j);
	SendMessage(hL, LVM_SETCOLUMNWIDTH, j, LVSCW_AUTOSIZE_USEHEADER);
	int a2 = ListView_GetColumnWidth(hL, j);
	if (a1 > a2)
		SendMessage(hL, LVM_SETCOLUMNWIDTH, j, LVSCW_AUTOSIZE);
	}

LPARAM ListGetParam(HWND hL, int L)
	{
	LV_ITEM ti = { 0 };
	ti.iItem = L;
	ti.mask = LVIF_PARAM;
	ListView_GetItem(hL, &ti);
	return ti.lParam;
	}

