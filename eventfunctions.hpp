// CreateEvent
HANDLE (WINAPI *TCreateEvent)(
	_In_opt_ LPSECURITY_ATTRIBUTES,
	_In_     BOOL,
	_In_     BOOL,
	_In_opt_ LPCTSTR);

HANDLE WINAPI MCreateEvent(
	_In_opt_ LPSECURITY_ATTRIBUTES lpEventAttributes,
	_In_     BOOL                  bManualReset,
	_In_     BOOL                  bInitialState,
	_In_opt_ LPCTSTR               lpName
)
	{
	DWORD tid = GetCurrentThreadId();
	if (tid == MyThread)
		return TCreateEvent(lpEventAttributes, bManualReset, bInitialState, lpName);

	PutThreads(true);
	auto h = TCreateEvent(lpEventAttributes, bManualReset, bInitialState, lpName);
	O o;
	o.ManualReset = bManualReset;
	o.Type = OBJECT_TYPE::TYPE_EVENT;
	o.State = OBJECT_STATE::TYPE_EV_RESET;
	if (bInitialState)
		o.State = OBJECT_STATE::TYPE_EV_SET;
	if (lpName)
		o.Name = lpName;
	o.LastChangeFrom = FindThread(tid);
	o.h = h;
	objs.w()->operator[](h) = o;
	UpdateDialog();
	return h;
	}

// CreateEventEx
HANDLE(WINAPI *TCreateEventEx)(
	_In_opt_ LPSECURITY_ATTRIBUTES lpEventAttributes,
	_In_opt_ LPCTSTR               lpName,
	_In_     DWORD                 dwFlags,
	_In_     DWORD                 dwDesiredAccess);

HANDLE WINAPI MCreateEventEx(
	_In_opt_ LPSECURITY_ATTRIBUTES lpEventAttributes,
	_In_opt_ LPCTSTR               lpName,
	_In_     DWORD                 dwFlags,
	_In_     DWORD                 dwDesiredAccess
)
	{
	DWORD tid = GetCurrentThreadId();
	if (tid == MyThread)
		return TCreateEventEx(lpEventAttributes, lpName, dwFlags, dwDesiredAccess);

	PutThreads(true);
	auto h = TCreateEventEx(lpEventAttributes, lpName, dwFlags, dwDesiredAccess);
	O o;
	o.ManualReset = false;
	if (dwFlags & CREATE_EVENT_MANUAL_RESET)
		o.ManualReset = true;
	o.Type = OBJECT_TYPE::TYPE_EVENT;
	o.State = OBJECT_STATE::TYPE_EV_RESET;
	if (dwFlags & CREATE_EVENT_INITIAL_SET)
		o.State = OBJECT_STATE::TYPE_EV_SET;
	if (lpName)
		o.Name = lpName;
	o.LastChangeFrom = FindThread(tid);
	o.h = h;
	objs.w()->operator[](h) = o;
	UpdateDialog();
	return h;
	}


// OpenEvent
HANDLE(WINAPI *TOpenEvent)(
	_In_ DWORD   dwDesiredAccess,
	_In_ BOOL    bInheritHandle,
	_In_ LPCTSTR lpName);

HANDLE WINAPI MOpenEvent(
	_In_ DWORD   dwDesiredAccess,
	_In_ BOOL    bInheritHandle,
	_In_ LPCTSTR lpName
)
	{
	DWORD tid = GetCurrentThreadId();
	if (tid == MyThread)
		return TOpenEvent(dwDesiredAccess, bInheritHandle, lpName);

	PutThreads(true);
	auto h = TOpenEvent(dwDesiredAccess, bInheritHandle, lpName);
	O o;
	o.ManualReset = 0;
	o.Type = OBJECT_TYPE::TYPE_EVENT;
	o.State = OBJECT_STATE::TYPE_EV_RESET;
	if (TWaitForSingleObject(h,0) == WAIT_OBJECT_0)
		o.State = OBJECT_STATE::TYPE_EV_SET;
	if (lpName)
		o.Name = lpName;
	o.LastChangeFrom = FindThread(tid);
	o.h = h;
	objs.w()->operator[](h) = o;
	UpdateDialog();
	return h;
	}


// SetEvent
DWORD(WINAPI *TSetEvent)(HANDLE);
DWORD  WINAPI MSetEvent(HANDLE hh)
	{
	DWORD tid = GetCurrentThreadId();
	if (tid == MyThread)
		return TSetEvent(hh);

	PutThreads(true);
	auto objf = FindObject(hh);
	if (objf)
		{
		objf->State = OBJECT_STATE::TYPE_EV_SET;
		PostMessage(hDlg, WM_USER + 1, tid, 0);
		}
	return TSetEvent(hh);
	}

// ResetEvent
DWORD(WINAPI *TResetEvent)(HANDLE);
DWORD  WINAPI MResetEvent(HANDLE hh)
	{
	DWORD tid = GetCurrentThreadId();
	if (tid == MyThread)
		return TResetEvent(hh);

	PutThreads(true);
	auto objf = FindObject(hh);
	if (objf)
		{
		objf->State = OBJECT_STATE::TYPE_EV_RESET;
		PostMessage(hDlg, WM_USER + 1, tid, 0);
		}
	return TSetEvent(hh);
	}


bool HookEvents()
	{
	// CreateEvent
	if (MH_CreateHook(&CreateEvent, &MCreateEvent, reinterpret_cast<LPVOID*>(&TCreateEvent)) != MH_OK) return false;
	if (MH_EnableHook(&CreateEvent) != MH_OK) return  false;

	// CreateEventEx
	if (MH_CreateHook(&CreateEventEx, &MCreateEventEx, reinterpret_cast<LPVOID*>(&TCreateEventEx)) != MH_OK) return false;
	if (MH_EnableHook(&CreateEventEx) != MH_OK) return  false;

	// OpenEvent
	if (MH_CreateHook(&OpenEvent, &MOpenEvent, reinterpret_cast<LPVOID*>(&TOpenEvent)) != MH_OK) return false;
	if (MH_EnableHook(&OpenEvent) != MH_OK) return  false;

	// SetEvent
	if (MH_CreateHook(&SetEvent, &MSetEvent, reinterpret_cast<LPVOID*>(&TSetEvent)) != MH_OK) return false;
	if (MH_EnableHook(&SetEvent) != MH_OK) return  false;

	// ResetEvent
	if (MH_CreateHook(&ResetEvent, &MResetEvent, reinterpret_cast<LPVOID*>(&TResetEvent)) != MH_OK) return false;
	if (MH_EnableHook(&ResetEvent) != MH_OK) return  false;

	return true;
	}

